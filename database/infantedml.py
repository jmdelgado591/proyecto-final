from database.conector import *
from modelo.infante import Infante

class DML():
    def getInfante(self):
        query = session.query(Infante)

        data = query.all()
        informacion = []
        for info in data:
            temp = {}
            temp['codigo'] = info.codigo
            temp['nombre'] = info.nombre
            temp['Fecha de Naciemiento'] = info.fechaN

            informacion.append(temp)
            print(temp)
        self.__cerraBD__()
        return informacion
    def addInfante(self, nom, fechaN):
        new = Infante(nombre=nom, fechaNacimiento=fechaN)

        session.add(new)
        session.commit()
        print("Nuevo Infante ", new)
        return new.serializar()

    def delInfante(self, id):
        query = session.query(Infante).filter_by(codigo=id)
        print('SQL ===> ', query)
        temp = query.first()
        print('RESULTADO ==> ', temp)

        if temp is None:
            return 'el usuario con el ID No existe '

        session.delete(temp)
        session.commit()
        self.__cerraBD__()
        return True
    def updateInfante(self, id, nom, fechaN):
        query = session.query(Infante).filter_by(codigo=id)
        print(query)
        temp = query.first()
        print('Resultado ', temp)

        temp.nombre = nom
        temp.fechaNacimiento = fechaN

        session.add(temp)
        session.commit()

        self.__cerraBD__()
        return True

    def __cerraBD__(self):
        session.close()





