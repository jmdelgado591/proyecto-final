from flask import Flask, jsonify, request
from database.infantedml import DML

app = Flask(__name__)
app.config['DEBUG'] = True

dml = DML()


@app.route('/', methods=['GET'])
def home():
    return "<h2> Lolas Day Care </h2>"

@app.route('/v1/infante', methods=['GET'])
def getApiInfante():
    data = dml.getInfante()
    return jsonify(data)


@app.route('/v1/infante', methods=['POST'])
def nana():
    print('Request info ==> ', request.args['nombre'])

    nombre = request.args['nombre']
    fechaN = request.args['Fecha de Naciemiento']
    print(nombre, fechaN)
    data = dml.addInfante(nombre, fechaN)
    return jsonify({'data': data})

@app.route('/v1/infante/<int:infante_id>', methods=['DELETE'])
def eliminarInfante(infante_id):
    print('Id a eliminar ', infante_id)
    data = dml.delInfante(infante_id)
    return jsonify({'status': data})

@app.route('/v1/infante/<int:nana_id>', methods=['PUT'])
def actualizarInfante(infante_id):
    print('Request info ==> ', request.args['nombre'])

    nombre = request.args['nombre']
    apellido = request.args['fecha de Nacimineto']

    data = dml.updateInfante(infante_id, nombre, apellido)
    return jsonify({'data': data})


