from flask import Flask, jsonify, request
from database.dml import DML

app = Flask(__name__)
app.config['DEBUG'] = True

dml = DML()


@app.route('/', methods=['GET'])
def home():
    return "<h2> Lolas Day Care </h2>"

@app.route('/v1/nana', methods=['GET'])
def getApiNana():
    data = dml.getNana()
    return jsonify(data)


@app.route('/v1/nana', methods=['POST'])
def nana():
    print('Request info ==> ', request.args['nombre'])

    nombre = request.args['nombre']
    direccion = request.args['direccion']
    print(nombre, direccion)
    data = dml.addNana(nombre, direccion)
    return jsonify({'data': data})

@app.route('/v1/nana/<int:nana_id>', methods=['DELETE'])
def eliminarNana(nana_id):
    print('Id a eliminar ', nana_id)
    data = dml.delNana(nana_id)
    return jsonify({'status': data})

@app.route('/v1/nana/<int:nana_id>', methods=['PUT'])
def actualizarNana(nana_id):
    print('Request info ==> ', request.args['nombre'])

    nombre = request.args['nombre']
    direccion = request.args['direccion']

    data = dml.updateNana(nana_id, nombre, direccion)
    return jsonify({'data': data})

