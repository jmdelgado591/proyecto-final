from database.conector import *
from modelo.infante import Infante, Nana
from servicio.api import *

def ejecutador_clase():

    Base.metadata.create_all(engine)

    nana1 = Nana (nombre="Ana Perez", direccion="San Francisco Calle 75")

    cl_p1 = Infante (nana=nana1, direccion="Calidonia", telefono="1234567", padre="Orlando Diaz")

    session.add_all([nana1, cl_p1])
    session.commit()

    result =  nana1.serializar()
    print(result)

if __name__ == '__main__':
    print("Lolas Guarderia")
    app.run()
