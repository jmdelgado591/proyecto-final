from database.conector import *
from sqlalchemy import  Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

class Infante (Base):
    __tablename__ = "infantes"
    codigo = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(40))
    fechaNacimiento = Column(String(10))
    padre = Column(String(40))
    madre = Column(String(40))


    nana_id = Column(Integer, ForeignKey('nanas.codigo'))
    nana = relationship('Nana')

class Nana (Base):
    __tablename__ = "nanas"
    codigo = Column(Integer, primary_key=True)
    nombre = Column(String(40))
    cedula = Column(String(15))
    direccion = Column(String(60))
    telefono = Column(String(25))


    nanas = relationship(Infante, backref="infantes")

    def serializar(self):
        data = {"id": self.codigo, "nombre": self.nombre, "cedula": self.cedula, "direccion": self.direccion, "telefono": self.telefono,}
        return data


